# FROM python:3.6
FROM centos:centos7.9.2009

WORKDIR /app

COPY . .

RUN yum install -y python3 python3-devel && \
    pip3 install --upgrade setuptools -i https://mirrors.aliyun.com/pypi/simple && \
    pip3 install --upgrade pip -i https://mirrors.aliyun.com/pypi/simple && \
    pip3 install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple

CMD [ "python3", "server.py"]
