# coding:utf-8 
import pyvisa as visa
import time
import logging
from pyvisa.resources.resource import Resource
import os

# from . import config
visa_lib = "/usr/lib/x86_64-linux-gnu/libvisa.so.20.0.0"
class ConnectionTest():

    def __init__(self):
        self.rm = visa.ResourceManager(visa_lib)
        logging.info("resource manager load success")


    @staticmethod
    def connection_test(self, resource_name):
        """传入资源名称
        """
        instr = self.rm.open_resource(resource_name)
        return instr


    def list_all_instruments(self):
        """例如出所有仪器名称
        """
        query_string = "visa://117.50.163.142:39003"
        list_of_instrs = self.rm.list_resources(query_string)
        logging.info("所有仪器:")
    
    def ohm(self):
        """
        *RST
FUNC “RES”
RES:MODE AUTO
RES:RANG 20E3
:SYST:RSEN ON
:FORM:ELEM RES
:OUTP ON
:READ?
:OUTP OFF
        """
        instr24 = self.rm.open_resource("visa://117.50.163.142:39003/GPIB0::24::INSTR")
        instr24.write("*RST")
        instr24.write("FUNC 'RES'")
        instr24.write("RES:RANG 20E3")
        instr24.write(":FORM:ELEM RES")
        instr24.write(":OUTP ON")
        res = instr24.query(":READ?")
        instr24.write(":OUTP OFF")
        print("resistance: {}".format(res))
        
        
    def basicSource(self):
        """
        *RST
:SOUR:FUNC VOLT
:SOUR:VOLT:MODE FIXED
:SOUR:VOLT:RANG 20
:SOUR:VOLT:LEV 10
:SENS:CURR:PROT 10E-3
:SENS:FUNC "CURR"
:SENS:CURR:RANG 10E-3
:FORM:ELEM CURR
:OUTP ON
:READ?
:OUTP OFF
        """
        instr = self.rm.open_resource("visa://117.50.163.142:39003/GPIB0::24::INSTR")
        instr.write('*rst')
        instr.write(":sour:func volt")
        instr.write(':SOUR:VOLT:MODE FIXED')
        instr.write(':SOUR:VOLT:RANG 20')
        instr.write(':sens:curr:prot 0.1')
        instr.write(':sens:func "curr:dc"')
        instr.write(':sens:nplc 5')
        
        instr.write(':trig:coun 1')
        instr.write(':trig:del 0')
        instr.write(':sour:del 0')
        instr.write(':outp on')
        
        instr.write(':sour:volt:lev 0.1')
        measure_i_info = instr.query(':read?') 
        print(measure_i_info)
        
        instr.write(":OUTP OFF")
        errs = self.get_errors(instr)
        print(errs)
        
    def get_errors(self, instr):
        """get errors counts and errors queue
        """
        error_count = instr.query(':syst:err:coun?')
        # return and clear all errors
        error_queue = instr.query(':syst:err:all?')
        
        return error_count, error_queue
    
    def instr_init(self, instr: Resource):
        """初始化2400

        """
        instr.write("*rst?")
        
        # 关闭蜂鸣器
        instr.write(":syst:beep:stat off")
        logging.info("BEEPER CLOSED")

        # 重置时间戳
        instr.write(":syst:time:res")
        timestamp = instr.query(":syst:time?")
        logging.info("TIMESTAMP RESETD: %s", timestamp)

    def IVSweepMode(self):
        """返回stream数据流
        """
        start_volt = -1
        end_volt = 1
        volt_step = 0.1
        current_cmpl = 100e-3
        mes_speed = 5

        instr2400 = self.rm.open_resource(instr_resource)
        logging.info("opend resource：%s", instr_resource)
        
        ## 配置扫描参数
        self.sweep_config(instr2400, current_cmpl, mes_speed)

        source_volt_level = start_volt
        while 1:
            instr2400.write(':sour:volt:lev %s' % (source_volt_level))
            measure_i_info = instr2400.query(':read?')

            logging.info(measure_i_info)

            source_volt_level += volt_step
            source_volt_level = round(source_volt_level, 5)
            end_volt = round(end_volt, 5)
            if source_volt_level > end_volt:
                break
        instr2400.write(":outp off")
        count, queue = self.get_errors(instr2400)
        logging.info(f"TOTAL ERRORS: {count} || {queue}")
    
    def sweep_config(self, instr:Resource, current_cmpl, mes_speed):
        """
        手册参考：https://download.tek.com/manual/2400S-900-01_K-Sep2011_User.pdf
        
        """
        self.instr_init(instr)
        # 配置source参数
        instr.write(":sour:func volt")
        instr.write(':SOUR:VOLT:MODE FIXED')
        instr.write(':SOUR:VOLT:RANG 20')
        # 配置sens参数
        instr.write(':sens:curr:prot %s' % current_cmpl)
        instr.write(':sens:func "curr:dc"')
        instr.write(':sens:nplc %d' % mes_speed)
        # 触发次数
        instr.write(':trig:coun 1')
        
        instr.write(':trig:del 0')
        instr.write(':sour:del 0')
        instr.write(':outp on')
    
    
    def test():
        ct = ConnectionTest()
    
        proxy_ip = "117.50.163.142:39003"
        ip = "202.197.78.43"
        
        proxy_instr = "visa://{ip}/GPIB0::24::INSTR".format(ip=proxy_ip)
        
        visa_lib = "/usr/lib/x86_64-linux-gnu/libvisa.so.20.0.0"
        rm = visa.ResourceManager(visa_lib)
        
        instr_resource = "visa://202.197.78.43/GPIB0::24::INSTR"
        instr24 = rm.open_resource(instr_resource)
        r = instr24.query("*idn?")
        print(r)

    
if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(name)s %(levelname)s %(message)s",
                    datefmt = '%Y-%m-%d  %H:%M:%S %a'   
                    )



    # ct.IVSweepMode()
    
    # ct.ohm()
    
    
    os.putenv("IP", "a")
    os.environ.setdefault("VISAIP","csumeslab.top:39003")
    
    
    
    print(os.getenv("VISAIP"))
   
    