# coding=utf-8
import os

ip = os.getenv("VISA_SERVER_IP") # ip_addr = "202.197.78.43" ip_addr_proxy = "117.50.163.142:39003"

visa_lib = "/usr/lib/x86_64-linux-gnu/libvisa.so.20.0.0"

test_instr_name = "visa://{addr}/GPIB0::INTFC".format(addr=ip)

instr_2400 = "visa://{addr}/GPIB0::24::INSTR".format(addr=ip)